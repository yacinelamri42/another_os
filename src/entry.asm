global loader
extern kmain
MAGIC_NUMBER equ 0x1BADB002             ; define the magic number constant
FLAGS equ 0x1                           ; multiboot flags, only 1 flag, to align modules
CHECKSUM equ -(MAGIC_NUMBER+FLAGS)      ; calculate the checksum
                                        ; (magic number + checksum + flags should equal 0)
section .text
align 4
    dd MAGIC_NUMBER
    dd FLAGS
    dd CHECKSUM

loader:
    mov esp, kernel_stack+KERNEL_STACK_SIZE
    push ebx
    push eax
    call kmain
.loop:
    jmp .loop


KERNEL_STACK_SIZE equ 0x4000            ; Set 16k of stack for kernel
section .bss
align 4                                 ; Align the stack by steps of 4 bytes
kernel_stack:
    resb KERNEL_STACK_SIZE
