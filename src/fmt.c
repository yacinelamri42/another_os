#include "fmt.h"

// char const * format(char * dest, char * const fmt, ...) {
//     u8 * current_element = (u8*)&fmt;
//     u32 i = 0; // index for the dest string
//     char * string = fmt;
//     while (*string!=0) {
//         char current_char = *string;
//         /*  bit 7-0
//          *  if bit 0 is set: the length is 1 byte
//          *  if bit 1 is set: the length is 2 bytes
//          *  if bit 2 is set: the length is 4 bytes
//          *  if bit 3 is set: the length is 8 bytes
//          *  The other 4 bits are reserved for future use
//          * */
//         u8 flags = 0; 
//         if (current_char == '%') {
//             string+=1;
//             // See if length specifier
//             switch (*string) {
//                 case 'h':
//                     string+=1;
//                     if (*(string+1) == 'h') {
//                         // i8 or u8
//                         flags = 1<<1;
//                         string+=1; // shifts the position of "cursor to the end of the formatter"
//                     }else {
//                         // i16 or u16
//                         flags = 1<<2;
//                     }
//                     break;
//                 case 'l':
//                     string+=1;
//                     if (*(string+1) == 'l') {
//                         // i64 or u64
//                         flags = 1 << 4;
//                         string+=1; // shifts the position of "cursor to the end of the formatter"
//                     }else {
//                         // i32 or u32
//                         flags = 1 << 3;
//                     }
//                     break;
//             }
//             // Look at formatter
//             switch (*string) {
//                 case 'd':
//                     // signed decimal integer
//                     switch (flags) {
//                         case 0:
//                             // normal int
//                             break;
//                         case 1:
//                             break;
//
//                         case 2:
//                             break;
//                     }
//                     
//                     break;
//                 case 'u':
//                     // unsigned decimal integer
//                     break;
//                 case 'x':
//                     // unsigned hex integer
//                     break;
//                 case 'c':
//                     // character
//                     break;
//                 case 's':
//                     // string
//                     break;
//                 case 'p':
//                     // pointer (u32)
//                     break;
//                 case '%':
//                     *(dest+i) = '%';
//                     i++;
//                     break;
//             }
//         }else {
//             *(dest+i) = current_char;
//             i++;
//         }
//         string++;
//     }
//     return dest;
//}
