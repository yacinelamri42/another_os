#ifndef FMT__H
#define FMT__H

#include "types.h"

char const * format(char * dest, char * const fmt, ...);

#endif
