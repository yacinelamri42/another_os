global load_gdt
extern log_info

load_gdt:
    lgdt [gdt_descriptor]
    mov ax, DATA_SEG
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    jmp CODE_SEG:flush_cs

flush_cs:
    mov eax, info
    push eax
    call log_info
    pop eax
    xor eax, eax
    ret

info: db "GDT has been loaded", 0

; GDT 
gdt_start:
    
gdt_null: ; mandatory null descriptor in gdt
    dd 0x0 ; dd means define double word
    dd 0x0

gdt_code_entry: ; code segment descriptor
    ; base(32) = 0x0
    ; limit(20) = 0xfffff
    ; present(1) = 1 (present means segment is present in memory)
    ; privilege(2) = 0 (means ring 0 (highest level))
    ; descriptor type(1) = 1 for code or data, 0 for traps
    ; type(4):
        ; code(1) = 1(means the segment is code)
        ; conforming(1) = 0 (if conforming, code with lower priviledge can access this memory area)
        ; readable(1) = 1 (means the ram can be used for data too instead of just code to execute)
        ; accessed(1) = 0 (this bit is changed by the cpu when we access this segment)
    ; granularity(1) = 1 (if this bit is set, multiplies limit by 16*16*16(4096))
    ; 32_bit default(1) = 1(this bit sets the default data size to 32bits)
    ; 64_bit segment(1) = 0(this bit sets the code to 64 bit)
    ; avl(1): 0 used for debugging but not used in example
    dw 0xffff ; limit first 16 bits
    dw 0x0000 ; base first 16 bits
    db 0x00 ; base next 8 bits
    db 10011010b ; flags present, privilege, descriptor type, type(code, conforming, readable, accessed)
    db 11001111b ; flags granularity, default 32_bit, 64 bit, avl, segment limit next 4 bits
    db 0x0
    
gdt_data_entry:; data segment descriptor
    ; same as gdt_code except the type
    ; type(4):
        ; code(1) = 0
        ; conforming(1) = 0
        ; readable(1) = 1
        ; accessed = 0
    dw 0xffff ; limit first 16 bits
    dw 0x0000 ; base first 16 bits
    db 0x00 ; base next 8 bits
    db 10010010b ; flags present, privilege, descriptor type, type(code, conforming, readable, accessed)
    db 11001111b ; flags granularity, default 32_bit, 64 bit, avl, segment limit next 4 bits
    db 0x0

gdt_end:
    ; the reason to put this tag is to help calculate the length of the code from gdt_start and gdt_end

gdt_descriptor:
    dw gdt_start - gdt_end - 1 ; the size of gdt is start - end - 1
    dd gdt_start ; write pointer to gdt data
; Constants that will be helpful later
CODE_SEG equ gdt_code_entry - gdt_start
DATA_SEG equ gdt_data_entry - gdt_start
