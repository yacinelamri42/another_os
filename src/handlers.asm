; Log
extern panic
extern log_info
extern log_error
extern log_debug

; Timer
extern timer_callback

; Keyboard
extern keypress_callback

; // Mandatory ints
global handler_int0
global handler_int1
global handler_int2
global handler_int3
global handler_int4
global handler_int5
global handler_int6
global handler_int7
global handler_int8
global handler_int9
global handler_int10
global handler_int11
global handler_int12
global handler_int13
global handler_int14
global handler_int15
global handler_int16
global handler_int17
global handler_int18
global handler_int19
global handler_int20
global handler_int21
; // Reserved
global handler_int22
global handler_int23
global handler_int24
global handler_int25
global handler_int26
global handler_int27
global handler_int28
global handler_int29
global handler_int30
global handler_int31
; // End of Reserved
; // End of mandatory
; // IRQs
global handler_int32
global handler_int33
global handler_int34
global handler_int35
global handler_int36
global handler_int37
global handler_int38
global handler_int39
global handler_int40
global handler_int41
global handler_int42
global handler_int43
global handler_int44
global handler_int45
global handler_int46
global handler_int47
; // Free interrupts
global handler_int48
global handler_int49
global handler_int50
global handler_int51
global handler_int52
global handler_int53
global handler_int54
global handler_int55
global handler_int56
global handler_int57
global handler_int58
global handler_int59
global handler_int60
global handler_int61
global handler_int62
global handler_int63
global handler_int64
global handler_int65
global handler_int66
global handler_int67
global handler_int68
global handler_int69
global handler_int70
global handler_int71
global handler_int72
global handler_int73
global handler_int74
global handler_int75
global handler_int76
global handler_int77
global handler_int78
global handler_int79
global handler_int80
global handler_int81
global handler_int82
global handler_int83
global handler_int84
global handler_int85
global handler_int86
global handler_int87
global handler_int88
global handler_int89
global handler_int90
global handler_int91
global handler_int92
global handler_int93
global handler_int94
global handler_int95
global handler_int96
global handler_int97
global handler_int98
global handler_int99
global handler_int100
global handler_int101
global handler_int102
global handler_int103
global handler_int104
global handler_int105
global handler_int106
global handler_int107
global handler_int108
global handler_int109
global handler_int110
global handler_int111
global handler_int112
global handler_int113
global handler_int114
global handler_int115
global handler_int116
global handler_int117
global handler_int118
global handler_int119
global handler_int120
global handler_int121
global handler_int122
global handler_int123
global handler_int124
global handler_int125
global handler_int126
global handler_int127
global handler_int128
global handler_int129
global handler_int130
global handler_int131
global handler_int132
global handler_int133
global handler_int134
global handler_int135
global handler_int136
global handler_int137
global handler_int138
global handler_int139
global handler_int140
global handler_int141
global handler_int142
global handler_int143
global handler_int144
global handler_int145
global handler_int146
global handler_int147
global handler_int148
global handler_int149
global handler_int150
global handler_int151
global handler_int152
global handler_int153
global handler_int154
global handler_int155
global handler_int156
global handler_int157
global handler_int158
global handler_int159
global handler_int160
global handler_int161
global handler_int162
global handler_int163
global handler_int164
global handler_int165
global handler_int166
global handler_int167
global handler_int168
global handler_int169
global handler_int170
global handler_int171
global handler_int172
global handler_int173
global handler_int174
global handler_int175
global handler_int176
global handler_int177
global handler_int178
global handler_int179
global handler_int180
global handler_int181
global handler_int182
global handler_int183
global handler_int184
global handler_int185
global handler_int186
global handler_int187
global handler_int188
global handler_int189
global handler_int190
global handler_int191
global handler_int192
global handler_int193
global handler_int194
global handler_int195
global handler_int196
global handler_int197
global handler_int198
global handler_int199
global handler_int200
global handler_int201
global handler_int202
global handler_int203
global handler_int204
global handler_int205
global handler_int206
global handler_int207
global handler_int208
global handler_int209
global handler_int210
global handler_int211
global handler_int212
global handler_int213
global handler_int214
global handler_int215
global handler_int216
global handler_int217
global handler_int218
global handler_int219
global handler_int220
global handler_int221
global handler_int222
global handler_int223
global handler_int224
global handler_int225
global handler_int226
global handler_int227
global handler_int228
global handler_int229
global handler_int230
global handler_int231
global handler_int232
global handler_int233
global handler_int234
global handler_int235
global handler_int236
global handler_int237
global handler_int238
global handler_int239
global handler_int240
global handler_int241
global handler_int242
global handler_int243
global handler_int244
global handler_int245
global handler_int246
global handler_int247
global handler_int248
global handler_int249
global handler_int250
global handler_int251
global handler_int252
global handler_int253
global handler_int254
global handler_int255

stop:
    jmp $

; // Mandatory ints
; Divide Error
handler_int0:
    pushad
    mov eax, divide_by_0_str
    push eax
    call log_error
    jmp stop
    popad
    iret

divide_by_0_str: db "Divide Error", 0

; Debug exception
handler_int1:
    pushad
    mov eax, debug_exception_str
    push eax
    call log_error
    jmp stop
    popad
    iret

debug_exception_str: db "Debug exception", 0

; NMI interrupt
handler_int2:
    pushad
    mov eax, debug_exception_str
    push eax
    call log_error
    popad
    iret

_NMI_interrupt: db " NMI interrupt", 0

; Breakpoint
handler_int3:
    pushad
    mov eax, breakpoint_str
    push eax
    call log_error
    jmp stop
    popad
    iret

breakpoint_str: db "Breakpoint", 0

; Overflow 
handler_int4:
    pushad
    mov eax, overflow_str
    push eax
    call log_error
    jmp stop
    popad
    iret

overflow_str: db "Overflow", 0

; BOUND range exceeded
handler_int5:
    pushad
    mov eax, BOUND_range_exceeded_str
    push eax
    call log_error
    jmp stop
    popad
    iret

BOUND_range_exceeded_str: db "BOUND range exceeded", 0

; Invalid opcode
handler_int6:
    pushad
    mov eax, invalid_opcode
    push eax
    call log_error
    jmp stop
    popad
    iret

invalid_opcode: db "Invalid opcode", 0

; Device not available(No math coprocessor)
handler_int7:
    pushad
    mov eax, no_math_coprocessor
    push eax
    call log_error
    jmp stop
    popad
    iret

no_math_coprocessor: db "No math coprocessor", 0

; Double fault
handler_int8:
    pushad
    mov eax, double_fault
    push eax
    call log_error
    jmp stop
    popad
    iret

double_fault: db "Double fault", 0

; Coprocessor Segment overrun
handler_int9:
    pushad
    mov eax, coprocessor_segment_overrun
    push eax
    call log_error
    jmp stop
    popad
    iret

coprocessor_segment_overrun: db "Coprocessor Segment overrun", 0

; Invalid Task segment switch
handler_int10:
    pushad
    mov eax, invalid_task_segment_switch
    push eax
    call log_error
    jmp stop
    popad
    iret

invalid_task_segment_switch: db "Invalid Task segment switch", 0

; Segment not present
handler_int11:
    pushad
    mov eax, segment_not_present
    push eax
    call log_error
    jmp stop
    popad
    iret

segment_not_present: db "Segment not present", 0

; Stack segment fault
handler_int12:
    pushad
    mov eax, stack_segment_fault
    push eax
    call log_error
    jmp stop
    popad
    iret

stack_segment_fault: db "Stack segment fault", 0

; General protection
handler_int13:
    pushad
    mov eax, general_protection
    push eax
    call log_error
    jmp stop
    popad
    iret

general_protection: db "General protection", 0

; Page fault
handler_int14:
    pushad
    mov eax, page_fault
    push eax
    call log_error
    jmp stop
    popad
    iret

page_fault: db "Page fault", 0

; Reserved
handler_int15:
    iret

; x87 FPU floating point error(Math error)
handler_int16:
    pushad
    mov eax, math_error
    push eax
    call log_error
    jmp stop
    popad
    iret

math_error: db "x87 FPU floating point error(Math error)", 0

; Alignement check
handler_int17:
    pushad
    mov eax, alignement_check
    push eax
    call log_error
    jmp stop
    popad
    iret

alignement_check: db "Alignement check", 0

; Machine check
handler_int18:
    pushad
    mov eax, machine_check
    push eax
    call log_error
    jmp stop
    popad
    iret

machine_check: db "Machine check", 0

; SIMD Floating point Exception
handler_int19:
    pushad
    mov eax, SIMD_floating_point_exception
    push eax
    call log_error
    jmp stop
    popad
    iret

SIMD_floating_point_exception: db "SIMD Floating point Exception", 0

; Virtualization Exception
handler_int20:
    pushad
    mov eax, virtualization_exception
    push eax
    call log_error
    jmp stop
    popad
    iret

virtualization_exception: db "Virtualization Exception", 0

; Control Protection Exception
handler_int21:
    pushad
    mov eax, control_protection_exception
    push eax
    call log_error
    jmp stop
    popad
    iret

control_protection_exception: db "Control Protection Exception", 0

; // Reserved
handler_int22:
    iret

handler_int23:
    iret

handler_int24:
    iret

handler_int25:
    iret

handler_int26:
    iret

handler_int27:
    iret

handler_int28:
    iret

handler_int29:
    iret

handler_int30:
    iret

handler_int31:
    iret

; // End of Reserved
; // End of mandatory
; // IRQs

; IRQ0
handler_int32:
    pusha
    push ds
    push es
    push fs
    push gs
    cli
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    ; mov eax, timer_callback
    ; push eax
    call timer_callback
    pop gs
    pop fs
    pop es
    pop ds
    sti
    popa
    iret

; IRQ1
handler_int33:
    pusha
    push ds
    push es
    push fs
    push gs
    cli
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    ; mov eax, timer_callback
    ; push eax
    call keypress_callback
    pop gs
    pop fs
    pop es
    pop ds
    sti
    popa
    iret

; IRQ2
handler_int34:
    iret

; IRQ3
handler_int35:
    iret

; IRQ4
handler_int36:
    iret

; IRQ5
handler_int37:
    iret

; IRQ6
handler_int38:
    iret

; IRQ7
handler_int39:
    iret

; IRQ8
handler_int40:
    iret

; IRQ9
handler_int41:
    iret

; IRQ10
handler_int42:
    iret

; IRQ11
handler_int43:
    iret

; IRQ12
handler_int44:
    iret

; IRQ13
handler_int45:
    iret

; IRQ14
handler_int46:
    iret

; IRQ15
handler_int47:
    iret

; // Free interrupts
handler_int48:
    iret

handler_int49:
    iret

handler_int50:
    iret

handler_int51:
    iret

handler_int52:
    iret

handler_int53:
    iret

handler_int54:
    iret

handler_int55:
    iret

handler_int56:
    iret

handler_int57:
    iret

handler_int58:
    iret

handler_int59:
    iret

handler_int60:
    iret

handler_int61:
    iret

handler_int62:
    iret

handler_int63:
    iret

handler_int64:
    iret

handler_int65:
    iret

handler_int66:
    iret

handler_int67:
    iret

handler_int68:
    iret

handler_int69:
    iret

handler_int70:
    iret

handler_int71:
    iret

handler_int72:
    iret

handler_int73:
    iret

handler_int74:
    iret

handler_int75:
    iret

handler_int76:
    iret

handler_int77:
    iret

handler_int78:
    iret

handler_int79:
    iret

handler_int80:
    iret

handler_int81:
    iret

handler_int82:
    iret

handler_int83:
    iret

handler_int84:
    iret

handler_int85:
    iret

handler_int86:
    iret

handler_int87:
    iret

handler_int88:
    iret

handler_int89:
    iret

handler_int90:
    iret

handler_int91:
    iret

handler_int92:
    iret

handler_int93:
    iret

handler_int94:
    iret

handler_int95:
    iret

handler_int96:
    iret

handler_int97:
    iret

handler_int98:
    iret

handler_int99:
    iret

handler_int100:
    iret

handler_int101:
    iret

handler_int102:
    iret

handler_int103:
    iret

handler_int104:
    iret

handler_int105:
    iret

handler_int106:
    iret

handler_int107:
    iret

handler_int108:
    iret

handler_int109:
    iret

handler_int110:
    iret

handler_int111:
    iret

handler_int112:
    iret

handler_int113:
    iret

handler_int114:
    iret

handler_int115:
    iret

handler_int116:
    iret

handler_int117:
    iret

handler_int118:
    iret

handler_int119:
    iret

handler_int120:
    iret

handler_int121:
    iret

handler_int122:
    iret

handler_int123:
    iret

handler_int124:
    iret

handler_int125:
    iret

handler_int126:
    iret

handler_int127:
    iret

handler_int128:
    iret

handler_int129:
    iret

handler_int130:
    iret

handler_int131:
    iret

handler_int132:
    iret

handler_int133:
    iret

handler_int134:
    iret

handler_int135:
    iret

handler_int136:
    iret

handler_int137:
    iret

handler_int138:
    iret

handler_int139:
    iret

handler_int140:
    iret

handler_int141:
    iret

handler_int142:
    iret

handler_int143:
    iret

handler_int144:
    iret

handler_int145:
    iret

handler_int146:
    iret

handler_int147:
    iret

handler_int148:
    iret

handler_int149:
    iret

handler_int150:
    iret

handler_int151:
    iret

handler_int152:
    iret

handler_int153:
    iret

handler_int154:
    iret

handler_int155:
    iret

handler_int156:
    iret

handler_int157:
    iret

handler_int158:
    iret

handler_int159:
    iret

handler_int160:
    iret

handler_int161:
    iret

handler_int162:
    iret

handler_int163:
    iret

handler_int164:
    iret

handler_int165:
    iret

handler_int166:
    iret

handler_int167:
    iret

handler_int168:
    iret

handler_int169:
    iret

handler_int170:
    iret

handler_int171:
    iret

handler_int172:
    iret

handler_int173:
    iret

handler_int174:
    iret

handler_int175:
    iret

handler_int176:
    iret

handler_int177:
    iret

handler_int178:
    iret

handler_int179:
    iret

handler_int180:
    iret

handler_int181:
    iret

handler_int182:
    iret

handler_int183:
    iret

handler_int184:
    iret

handler_int185:
    iret

handler_int186:
    iret

handler_int187:
    iret

handler_int188:
    iret

handler_int189:
    iret

handler_int190:
    iret

handler_int191:
    iret

handler_int192:
    iret

handler_int193:
    iret

handler_int194:
    iret

handler_int195:
    iret

handler_int196:
    iret

handler_int197:
    iret

handler_int198:
    iret

handler_int199:
    iret

handler_int200:
    iret

handler_int201:
    iret

handler_int202:
    iret

handler_int203:
    iret

handler_int204:
    iret

handler_int205:
    iret

handler_int206:
    iret

handler_int207:
    iret

handler_int208:
    iret

handler_int209:
    iret

handler_int210:
    iret

handler_int211:
    iret

handler_int212:
    iret

handler_int213:
    iret

handler_int214:
    iret

handler_int215:
    iret

handler_int216:
    iret

handler_int217:
    iret

handler_int218:
    iret

handler_int219:
    iret

handler_int220:
    iret

handler_int221:
    iret

handler_int222:
    iret

handler_int223:
    iret

handler_int224:
    iret

handler_int225:
    iret

handler_int226:
    iret

handler_int227:
    iret

handler_int228:
    iret

handler_int229:
    iret

handler_int230:
    iret

handler_int231:
    iret

handler_int232:
    iret

handler_int233:
    iret

handler_int234:
    iret

handler_int235:
    iret

handler_int236:
    iret

handler_int237:
    iret

handler_int238:
    iret

handler_int239:
    iret

handler_int240:
    iret

handler_int241:
    iret

handler_int242:
    iret

handler_int243:
    iret

handler_int244:
    iret

handler_int245:
    iret

handler_int246:
    iret

handler_int247:
    iret

handler_int248:
    iret

handler_int249:
    iret

handler_int250:
    iret

handler_int251:
    iret

handler_int252:
    iret

handler_int253:
    iret

handler_int254:
    iret

handler_int255:
    iret






