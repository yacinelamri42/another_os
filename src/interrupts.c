#include "interrupts.h"
#include "memutils.h"
#include "load_idt.h"
#include "log.h"

#define NUM_INTERRUPTS 256
static idt_entry_t interrupts[NUM_INTERRUPTS];
idt_descriptor_t idt_descriptor;

// typedef enum {
//     Interrupt,
//     Task,
//     Fault,
//     Abort,
//     Trap
// } InterruptType;

void interrupts_init() {
    log_info("Interrupts has been initialised");
    memory_set(interrupts, 0, 512);
    add_handler(0, (u32)handler_int0);   
    add_handler(1, (u32)handler_int1);   
    add_handler(2, (u32)handler_int2);   
    add_handler(3, (u32)handler_int3);   
    add_handler(4, (u32)handler_int4);   
    add_handler(5, (u32)handler_int5);   
    add_handler(6, (u32)handler_int6);   
    add_handler(7, (u32)handler_int7);   
    add_handler(8, (u32)handler_int8);   
    add_handler(9, (u32)handler_int9);   
    add_handler(10, (u32)handler_int10);   
    add_handler(11, (u32)handler_int11);   
    add_handler(12, (u32)handler_int12);   
    add_handler(13, (u32)handler_int13);   
    add_handler(14, (u32)handler_int14);   
    add_handler(15, (u32)handler_int15);   
    add_handler(16, (u32)handler_int16);   
    add_handler(17, (u32)handler_int17);   
    add_handler(18, (u32)handler_int18);   
    add_handler(19, (u32)handler_int19);   
    add_handler(20, (u32)handler_int20);   
    add_handler(21, (u32)handler_int21);   
    add_handler(22, (u32)handler_int22);   
    add_handler(23, (u32)handler_int23);   
    add_handler(24, (u32)handler_int24);   
    add_handler(25, (u32)handler_int25);   
    add_handler(26, (u32)handler_int26);   
    add_handler(27, (u32)handler_int27);   
    add_handler(28, (u32)handler_int28);   
    add_handler(29, (u32)handler_int29);   
    add_handler(30, (u32)handler_int30);   
    add_handler(31, (u32)handler_int31);   
    idt_descriptor.base = (u32)&interrupts;
    idt_descriptor.limit = NUM_INTERRUPTS * sizeof(idt_descriptor) - 1;
    load_idt((u32)&idt_descriptor);
}

void add_handler(/*InterruptType type,*/ u8 int_num, u32 handler) {
    interrupts[int_num].reserved = 0;
    interrupts[int_num].handler_l = (u16)(handler & 0xffff);
    interrupts[int_num].handler_h = (u16)(handler>>16 & 0xffff);
    interrupts[int_num].segment_selector = 0x08;
    interrupts[int_num].flags = 0b10001110;
}


