#ifndef INTERRUPTS__H
#define INTERRUPTS__H

#include "types.h"
#include "handlers.h"

typedef enum {
    Interrupt,
    Task,
    Fault,
    Abort,
    Trap
} InterruptType;

typedef struct {
    u16 handler_l;
    u16 segment_selector;
    u8 reserved;
    u8 flags;
    u16 handler_h;
} __attribute__((packed)) idt_entry_t ;

typedef struct {
    u16 limit;
    u32 base;
} __attribute__((packed)) idt_descriptor_t;


void interrupts_init();
void add_handler(/*InterruptType type,*/ u8 int_num, u32 handler);
#endif
