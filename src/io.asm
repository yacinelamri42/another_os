global outb
global outw
global inb
global inw
global io_wait

; outb: Send a byte to an I/O port
; Paramaters:   [esp+8]: the data byte
;               [esp+4]: The port
;               [esp]: The return address (Dont mess with it)
outb:
    mov al, [esp+8]
    mov dx, [esp+4]
    out dx, al
    ret

; outw: Send a word(2 bytes) to an I/O port
; Paramaters:   [esp+8]: the data bytes
;               [esp+4]: The port
;               [esp]: The return address (Dont mess with it)
outw:
    mov ax, [esp+8]
    mov dx, [esp+4]
    out dx, ax
    ret

; inb: Receive a byte from an I/O port
; Paramaters:   [esp+4]: The port
;               [esp]: The return address (Dont mess with it)
inb:
    mov dx, [esp+4]
    in al, dx
    ret

; inw: Receive a word(2 bytes) from an I/O port
; Paramaters:   [esp+4]: The port
;               [esp]: The return address (Dont mess with it)
inw:
    mov dx, [esp+4]
    in ax, dx
    ret

 ; io_wait: waits enought time to let the other port instructions to finish
 ; Paramaters:  [esp]: return address
 ;
 io_wait:
    mov al, 0
    out 0x80, al
    ret
