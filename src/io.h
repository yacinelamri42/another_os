#ifndef IO__H
#define IO__H

#include "types.h"

/* outb: Send a byte to an I/O port
 Paramaters:   [esp+8]: the data byte
                [esp+4]: The port
               [esp]: The return address (Dont mess with it)*/
void outb(u16 port, u8 data);

/*
 outw: Send a word(2 bytes) to an I/O port
 Paramaters:   [esp+8]: the data bytes
               [esp+4]: The port
               [esp]: The return address (Dont mess with it)
 */
void outw(u16 port, u16 data);

/*
 inw: Receive a word(2 bytes) from an I/O port
 Paramaters:   [esp+4]: The port
               [esp]: The return address (Dont mess with it)
*/
u8 inb(u16 port);

/*
 inw: Receive a word(2 bytes) from an I/O port
 Paramaters:   [esp+4]: The port
               [esp]: The return address (Dont mess with it)
*/
u16 inw(u16 port);

void io_wait();
#endif
