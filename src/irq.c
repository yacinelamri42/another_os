#include "irq.h"
#include "handlers.h"
#include "interrupts.h"

void install_irqs() {
    add_handler(IRQ0, (u32)handler_int32);
    add_handler(IRQ1, (u32)handler_int33);
    add_handler(IRQ2, (u32)handler_int34);
    add_handler(IRQ3, (u32)handler_int35);
    add_handler(IRQ4, (u32)handler_int36);
    add_handler(IRQ5, (u32)handler_int37);
    add_handler(IRQ6, (u32)handler_int38);
    add_handler(IRQ7, (u32)handler_int39);
    add_handler(IRQ8, (u32)handler_int40);
    add_handler(IRQ9, (u32)handler_int41);
    add_handler(IRQ10, (u32)handler_int42);
    add_handler(IRQ11, (u32)handler_int43);
    add_handler(IRQ12, (u32)handler_int44);
    add_handler(IRQ13, (u32)handler_int45);
    add_handler(IRQ14, (u32)handler_int46);
    add_handler(IRQ15, (u32)handler_int47);
}
