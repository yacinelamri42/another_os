#include "gdt.h"
#include "interrupts.h"
#include "keyboard.h"
#include "pic.h"
#include "timer.h"
#include "irq.h"
#include "types.h"
#include "vga.h"
#include "multiboot.h"
#include "string.h"
#include "module.h"

void initialise_kernel(void);
void print_total_memory( multiboot_info_t * multiboot_header_pointer);
void load_module(multiboot_info_t * multiboot_header_pointer);

void kmain(u32 multiboot_magic_num, multiboot_info_t * multiboot_header_pointer) {
    if (multiboot_magic_num != MULTIBOOT_BOOTLOADER_MAGIC) {
        kprint("Wrong multiboot header!");
        return;
    }
    
    // load_module(multiboot_header_pointer);
    print_total_memory(multiboot_header_pointer);
    initialise_kernel();
    while (true) {
	kprint(">");
	char buffer[256];
	get_input(buffer, 256);
	if (string_compare(buffer, "HALT")) {
	    break;
	}else if (string_compare(buffer, "RED")) {
	    change_screen_color_bg(Red);
	}
	kprint("\n");
    }
    kprint("\nGoodbye");
    #ifdef QEMU
    
    outw(0x604, 0x2000);
    #endif /* ifdef QEMU
     */
}

// void load_module(multiboot_info_t * multiboot_header_pointer) {
//     if (multiboot_header_pointer ->mods_count < 1) {
//         return;
//     }
//     int (*module)() = (int(*)()) multiboot_header_pointer->mods_addr;
//     int a = module();
//     char buf[256];
//     i32_to_ascii(buf, a);
//     kprint(buf);
// }

void initialise_kernel(void) {
    load_gdt();
    interrupts_init();
    PIC_remap(0x20, 0x28);
    install_irqs();
    init_timer(1000);
}

void print_total_memory( multiboot_info_t * multiboot_header_pointer) {
    char buf[256]; 
    i32_to_ascii(buf, multiboot_header_pointer->mem_upper);
    kprint("High memory: ");
    kprint(buf);
    kprint("\n");
}
