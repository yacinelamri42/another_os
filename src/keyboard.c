#include "keyboard.h"
#include "keyboard_scancodes.h"
#include "pic.h"
#include "irq.h"
#include "io.h"
#include "log.h"
#include "vga.h"
#include "memutils.h"
#include "string.h"

char last_char = '\0';
i8 scancode = 0;
bool key_pressed = false;

void keypress_callback() {
    PIC_send_EOI(IRQ1);
    u8 status_code = inb(KEYBOARD_STATUS_PORT);
    if (status_code & 1) {
        scancode = inb(KEYBOARD_DATA_PORT);
        if (scancode < 0) {
            return;
        }
        key_pressed = true;
        // log_debug("Key pressed");
    
    }
    read_scancode();
}

u8 get_current_scancode() {
    return scancode;
}

void read_scancode() {
    switch (scancode) {
        case KEY_1:
            last_char = '1';
            break;
        case KEY_2:
            last_char = '2';
            break;
        case KEY_3:
            last_char = '3';
            break;
        case KEY_4:
            last_char = '4';
            break;
        case KEY_5:
            last_char = '5';
            break;
        case KEY_6:
            last_char = '6';
            break;
        case KEY_7:
            last_char = '7';
            break;
        case KEY_8:
            last_char = '8';
            break;
        case KEY_9:
            last_char = '9';
            break;
        case KEY_0:
            last_char = '0';
            break;
        case KEY_A:
            last_char = 'A';
            break;
        case KEY_B:
            last_char = 'B';
            break;
        case KEY_C:
            last_char = 'C';
            break;
        case KEY_D:
            last_char = 'D';
            break;
        case KEY_E:
            last_char = 'E';
            break;
        case KEY_F:
            last_char = 'F';
            break;
        case KEY_G:
            last_char = 'G';
            break;
        case KEY_H:
            last_char = 'H';
            break;
        case KEY_I:
            last_char = 'I';
            break;
        case KEY_J:
            last_char = 'J';
            break;
        case KEY_K:
            last_char = 'K';
            break;
        case KEY_L:
            last_char = 'L';
            break;
        case KEY_M:
            last_char = 'M';
            break;
        case KEY_N:
            last_char = 'N';
            break;
        case KEY_O:
            last_char = 'O';
            break;
        case KEY_P:
            last_char = 'P';
            break;
        case KEY_Q:
            last_char = 'Q';
            break;
        case KEY_R:
            last_char = 'R';
            break;
        case KEY_S:
            last_char = 'S';
            break;
        case KEY_T:
            last_char = 'T';
            break;
        case KEY_U:
            last_char = 'U';
            break;
        case KEY_V:
            last_char = 'V';
            break;
        case KEY_W:
            last_char = 'W';
            break;
        case KEY_X:
            last_char = 'X';
            break;
        case KEY_Y:
            last_char = 'Y';
            break;
        case KEY_Z:
            last_char = 'Z';
            break;
        case KEY_DOT:
            last_char = '.';
            break;
        case KEY_SEMICOLON:
            last_char = ';';
            break;
        case KEY_SPACE:
            last_char = ' ';
            break;
        case KEY_BACKSPACE:
            last_char = '\b';
            break;
        case KEY_ENTER:
            last_char = '\n';
            break;
        case KEY_UP:
            break;
        case KEY_DOWN:
            break;
        case KEY_LEFT:
            break;
        case KEY_RIGHT:
            break;
    }
}

void get_input(char *buffer, u32 len) {
    u32 index;
    u16 pos = get_cursor_position();
    for (index = 0; index < len-1; index++) {
	while (!key_pressed) {
	    io_wait();
	} // wait until the key is pressed
	char key = get_current_char();
	key_pressed = false;
	if (key == '\n') {
	    break;
	}else if (key == '\b') {
	    if (index >= 0) {
		index--;
		*(buffer+index) = '\0';
	    }
	}else{
	    *(buffer+index) = key;
	    *(buffer+index+1) = '\0';
	    print_char(key);
	}
    }
    *(buffer+index) = '\0';
}

u8 get_current_char() {
    return last_char;
}
