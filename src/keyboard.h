#ifndef KEYBOARD__H
#define KEYBOARD__H

#include "types.h"
#include "keyboard_scancodes.h"

#define KEYBOARD_DATA_PORT 0x60
#define KEYBOARD_COMMAND_PORT 0x64
#define KEYBOARD_STATUS_PORT 0x64


void init_keyboard();
void keypress_callback();
u8 get_current_scancode();
u8 get_current_char();
void read_scancode();

void get_input(char * buffer, u32 len);

#endif
