[bits 32]
global load_idt

; (u32 descriptor)
load_idt:
    mov  eax, [esp+4]
    lidt [eax]
    sti
    ret
