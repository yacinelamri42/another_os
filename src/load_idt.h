#ifndef LOAD_IDT__H
#define LOAD_IDT__H

#include "types.h"
extern void load_idt(u32 descriptor);

#endif
