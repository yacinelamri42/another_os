#include "log.h"
#include "serial_ports.h"
#include "vga.h"


/* sends an information to the log
 *
 * @param string    The information needed to be sent
 * */
void log_info(const char * string) {
    serial_puts(SERIAL_PORT_COM1_BASE, "[INIT] ");
    serial_puts(SERIAL_PORT_COM1_BASE, string);
    serial_puts(SERIAL_PORT_COM1_BASE, "\n");
}

/*  Sends debug information to the log when the DEBUG preprocessor macro is enabled
 *
 *  @param string   The debug info
 * */
void log_debug(const char * string) {
    serial_puts(SERIAL_PORT_COM1_BASE, "[DEBUG] ");
    serial_puts(SERIAL_PORT_COM1_BASE, string);
    serial_puts(SERIAL_PORT_COM1_BASE, "\n");
}

/*  Sends a recoverable error information without crashing the os/program
 *
 *  @param string   The error info
 *
 * */
void log_error(const char * string) {
    serial_puts(SERIAL_PORT_COM1_BASE, "[ERROR] ");
    serial_puts(SERIAL_PORT_COM1_BASE, string);
    serial_puts(SERIAL_PORT_COM1_BASE, "\n");
}

/*  Crashes the program and sends the string to the screen and the log
 *
 *  @param string   The panic string
 * */
void panic(const char * string) {
    serial_puts(SERIAL_PORT_COM1_BASE, "[PANIC] ");
    serial_puts(SERIAL_PORT_COM1_BASE, string);
    // serial_puts(SERIAL_PORT_COM1_BASE, "\n");
    kprint("[PANIC] ");
    kprint(string);
    __asm__("hlt");
}
