#ifndef LOG__H
#define LOG__H


/* sends an information to the log
 *
 * @param string    The information needed to be sent
 * */
void log_info(const char * string);

/*  Sends debug information to the log when the DEBUG preprocessor macro is enabled
 *
 *  @param string   The debug info
 * */
void log_debug(const char * string);

/*  Sends a recoverable error information without crashing the os/program
 *
 *  @param string   The error info
 *
 * */
void log_error(const char * string);

/*  Crashes the program and sends the string to the screen and the log
 *
 *  @param string   The panic string
 * */
void panic(const char * string);

#endif

