#include "memutils.h"

void* memory_copy(void* destination, void* source, u64 length) {
    for (u64 i=0; i<length; i++) {
        *(u8*)(destination+i) = *(u8*)(source+i);
    }
    return destination;
}


void* memory_set(void* destination, u32 value, u64 size) {
    for (u64 i=0; i<size; i+=4) {
        *(u32*) (destination+i) = value;
    }
    return destination;
}
