#ifndef MEMUTILS__H
#define MEMUTILS__H

#include "types.h"

void* memory_copy(void* destination, void* source, u64 length);
void* memory_set(void* destination, u32 value, u64 length);

#endif
