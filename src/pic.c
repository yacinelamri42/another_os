#include "pic.h"
#include "io.h"
#include "log.h"

 
/* Helper func */
static u16 PIC_get_irq_register(int ocw3) {
    /* OCW3 to PIC CMD to get the register values.  PIC2 is chained, and
     * represents IRQs 8-15.  PIC1 is IRQs 0-7, with 2 being the chain */
    outb(MASTER_PIC_COMMAND_PORT, ocw3);
    outb(SLAVE_PIC_COMMAND_PORT, ocw3);
    return (inb(SLAVE_PIC_COMMAND_PORT) << 8) | inb(MASTER_PIC_COMMAND_PORT);
}
 
/* Returns the combined value of the cascaded PICs irq request register */
u16 pic_get_irr() {
    return PIC_get_irq_register(PIC_READ_IRR);
}
 
/* Returns the combined value of the cascaded PICs in-service register */
u16 pic_get_isr(void) {
    return PIC_get_irq_register(PIC_READ_ISR);
}

void PIC_send_EOI(u8 irq) {
    if (irq >= 8) {
        outb(SLAVE_PIC_COMMAND_PORT, PIC_EOI);
    }
    outb(MASTER_PIC_COMMAND_PORT, PIC_EOI);
}

void PIC_remap(u8 offset_master, u8 offset_slave) {
	unsigned char a1, a2;
 
	a1 = inb(MASTER_PIC_DATA_PORT);                        // save masks
	a2 = inb(SLAVE_PIC_DATA_PORT);
 
	outb(MASTER_PIC_COMMAND_PORT, ICW1_INIT | ICW1_ICW4);  // starts the initialization sequence (in cascade mode)
	io_wait();
	outb(SLAVE_PIC_COMMAND_PORT, ICW1_INIT | ICW1_ICW4);
	io_wait();
	outb(MASTER_PIC_DATA_PORT, offset_master);                 // ICW2: Master PIC vector offset
	io_wait();
	outb(SLAVE_PIC_DATA_PORT, offset_slave);                 // ICW2: Slave PIC vector offset
	io_wait();
	outb(MASTER_PIC_DATA_PORT, 4);                       // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	io_wait();
	outb(SLAVE_PIC_DATA_PORT, 2);                       // ICW3: tell Slave PIC its cascade identity (0000 0010)
	io_wait();
 
	outb(MASTER_PIC_DATA_PORT, ICW4_8086);               // ICW4: have the PICs use 8086 mode (and not 8080 mode)
	io_wait();
	outb(SLAVE_PIC_DATA_PORT, ICW4_8086);
	io_wait();
 
	outb(MASTER_PIC_DATA_PORT, a1);   // restore saved masks.
	outb(SLAVE_PIC_DATA_PORT, a2);

    log_info("PIC has been remaped");

}

void IRQ_set_mask(u8 IRQline) {
    u16 port;
    u16 value;
 
    if(IRQline < 8) {
        port = MASTER_PIC_DATA_PORT;
    } else {
        port = SLAVE_PIC_DATA_PORT;
        IRQline -= 8;
    }
    value = inb(port) | (1 << IRQline);
    outb(port, value);        
}
 
void IRQ_clear_mask(u8 IRQline) {
    u16 port;
    u8 value;
 
    if(IRQline < 8) {
        port = MASTER_PIC_DATA_PORT;
    } else {
        port = SLAVE_PIC_DATA_PORT;
        IRQline -= 8;
    }
    value = inb(port) & ~(1 << IRQline);
    outb(port, value);        
}
