#include "serial_ports.h"
#include "io.h"

// Initialise the port 
// @param serial_com_port
void serial_init(u16 serial_com_port) {
    serial_configure_baud_rate(serial_com_port, 0x2);
    serial_configure_line(serial_com_port);
    serial_configure_fifo_buffer(serial_com_port);
    serial_configure_modem(serial_com_port);
}

/* Sets the rate at which the data is sent and received in the serial COM
 * @param serial_com_port   This is the port to configure
 * @param divisor           This rate is equal to 115200/divisor, the divisor can be set to any number, the larger the divisor, the slower the speed, the lower the divisor, the less reliable the connection
 * */
void serial_configure_baud_rate(u16 serial_com_port, u16 divisor) {
    outb(SERIAL_LINE_COMMAND_PORT(serial_com_port), SERIAL_LINE_ENABLE_DLAB); // Enable the 16 bit mode
    outb(SERIAL_DATA_PORT(serial_com_port), (u8)(divisor >> 8) & 0xff); // Send the first 8 high bits of the divisor
    outb(SERIAL_DATA_PORT(serial_com_port), (u8)(divisor & 0xff)); // Send the lower 8 bits of the divisor
}

/* This function sets the configuration of the line:
 *  8 bits sent, no parity bits, 1 stop bit
 *
 *
 * 
 *  Bit:        | 7 | 6 | 5 4 3 | 2 | 1 0 |
 *  Content:    | d | b | prty | s | dl |
 *  Value:      | 0 | 0 | 0 0 0 | 0 | 1 1 | = 0x03
 */
void serial_configure_line(u16 serial_com_port) {
    outb(SERIAL_LINE_COMMAND_PORT(serial_com_port), 0x03);
}


/* Bit:     | 7 6 | 5  | 4 | 3 | 2 | 1 | 0 |
 * Content: | lvl | bs | r |dma|clt|clr| e |
 * Value: 0xC7
 *
 *  This function sets the configuration of the FIFO buffer to enable FIFO, clear both transmitter and receiver FIFO queues and use 14 bytes as the size of queue
 *
 * @param serial_com_port This is the port which to configure
 *
 * */
void serial_configure_fifo_buffer(u16 serial_com_port) {
    outb(SERIAL_FIFO_COMMAND_PORT(serial_com_port), 0xc7);
}

/*  Bit:     | 7 | 6 | 5  | 4  | 3   | 2   | 1   | 0   |
 *  Content: | r | r | af | lb | ao2 | ao1 | rts | dtr |
 *  Value: 0x3
 *
 *  This function sets the configuration of the modem to say that we are ready to transmit the data
 *
 * @param serial_com_port This is the port which to configure
 * */
void serial_configure_modem(u16 serial_com_port) {
    outb(SERIAL_MODEM_COMMAND_PORT(serial_com_port), 0x3);
}

/*  Checks whether the FIFO queues are empty by verifying the 5th bit 
 *
 *  @param serial_com_port The com port to verify
 *  @returns 1 is empty and 0 if full
 *
 *
 * */
u8 serial_is_fifo_empty(u16 serial_com_port) {
    return inb(SERIAL_LINE_STATUS_PORT(serial_com_port)) & 0b00100000;
}

/*  Sends a character c to a serial com port
 *
 *  @param serial com port : The port where the data is sent
 *  @c                       The character sent
 *
 *
 * */
void serial_putchar(u16 serial_com_port, char c) {
    while (serial_is_fifo_empty(SERIAL_DATA_PORT(serial_com_port)) == 0) {}
    outb(SERIAL_DATA_PORT(serial_com_port), c);
}


/* sends a string to the serial com port
 *
 * @param serial_com_port : The serial com port where the data is sent
 * @param string:           The pointer to the string to print
 *
 * */
void serial_puts(u16 serial_com_port, const char * string) {
    while (*string != 0) {
        serial_putchar(serial_com_port, *string);
        string++;
    }
}
