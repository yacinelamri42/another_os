#ifndef SERIAL_PORTS__H
#define SERIAL_PORTS__H

#include "types.h"

#define SERIAL_PORT_COM1_BASE 0x3f8
#define SERIAL_PORT_COM2_BASE 0x2f8

/* All the I/O ports are calculated relative to the data port. This is because
* all serial ports (COM1, COM2, COM3, COM4) have their ports in the same
* order, but they start at different values.
*/
#define SERIAL_DATA_PORT(base)          (base)
#define SERIAL_FIFO_COMMAND_PORT(base)  (base + 2)
#define SERIAL_LINE_COMMAND_PORT(base)  (base + 3)
#define SERIAL_MODEM_COMMAND_PORT(base) (base + 4)
#define SERIAL_LINE_STATUS_PORT(base)   (base + 5)

/* SERIAL_LINE_ENABLE_DLAB:
* Tells the serial port to expect first the highest 8 bits on the data port,
* then the lowest 8 bits will follow
*/
#define SERIAL_LINE_ENABLE_DLAB 0x80

/* Sets the rate at which the data is sent and received in the serial COM
 * @param serial_com_port   This is the port to configure
 * @param divisor           This rate is equal to 115200/divisor, the divisor can be set to any number, the larger the divisor, the slower the speed, the lower the divisor, the less reliable the connection
 * */
void serial_configure_baud_rate(u16 serial_com_port, u16 divisor);

/* This function sets the configuration of the line:
 *  8 bits sent, no parity bits, 1 stop bit
 *
 * @param serial_com_port This is the port which to configure
 * 
 *  Bit:        | 7 | 6 | 5 4 3 | 2 | 1 0 |
 *  Content:    | d | b | prty | s | dl |
 *  Value:      | 0 | 0 | 0 0 0 | 0 | 1 1 | = 0x03
 */
void serial_configure_line(u16 serial_com_port);

/* Bit:     | 7 6 | 5  | 4 | 3 | 2 | 1 | 0 |
 * Content: | lvl | bs | r |dma|clt|clr| e |
 * Value: 0xC7
 *
 *  This function sets the configuration of the FIFO buffer to enable FIFO, clear both transmitter and receiver FIFO queues and use 14 bytes as the size of queue
 *
 * @param serial_com_port This is the port which to configure
 *
 * */
void serial_configure_fifo_buffer(u16 serial_com_port);

/*  Bit:     | 7 | 6 | 5  | 4  | 3   | 2   | 1   | 0   |
 *  Content: | r | r | af | lb | ao2 | ao1 | rts | dtr |
 *  Value: 0x3
 *
 *  This function sets the configuration of the modem to say that we are ready to transmit the data
 *
 * @param serial_com_port This is the port which to configure
 * */
void serial_configure_modem(u16 serial_com_port); 

/*  Checks whether the FIFO queues are empty
 *
 *  @param serial_com_port The com port to verify
 *  @returns 1 is empty and 0 if full
 *
 *
 * */
u8 serial_is_fifo_empty(u16 serial_com_port);

/*  Sends a character c to a serial com port
 *
 *  @param serial com port : The port where the data is sent
 *  @c                       The character sent
 *
 *
 * */
void serial_putchar(u16 serial_com_port, char c);

/* sends a string to the serial com port
 *
 * @param serial_com_port : The serial com port where the data is sent
 * @param string:           The pointer to the string to print
 *
 * */
void serial_puts(u16 serial_com_port, const char * string);

// Initialise the port 
// @param serial_com_port
void serial_init(u16 serial_com_port);
#endif
