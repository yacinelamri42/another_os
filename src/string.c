#include "string.h"
#include "types.h"

u32 string_length(char const * string) {
    u32 len = 0;
    while (*(string+len)!=0) {
        len+=1;
    }
    return len;
}

/* K&R */
void reverse_string(char s[]) {
    int c, i, j;
    for (i = 0, j = string_length(s)-1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void i32_to_ascii(char str[], i32 n) {
    int i, sign;
    if ((sign = n) < 0) n = -n;
    i = 0;
    do {
        str[i++] = n % 10 + '0';
    } while ((n /= 10) > 0);

    if (sign < 0) str[i++] = '-';
    str[i] = '\0';

    reverse_string(str);
}
//
// void u32_to_ascii(char * string, i32 num) {
//     
// }
//
bool string_compare(const char * string1, const char * string2) {
    if (string_length(string1) != string_length(string2)) {
	return false;
    }else {
	for (int i=0; i<string_length(string1); i++) {
	    if (*(string1+i) != *(string2+i)) {
		return false;
	    }
	}
	return true;
    }
}
