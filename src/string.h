#ifndef STRING__H
#define STRING__H

#include "types.h"

u32 string_length(char const * string);
void i32_to_ascii(char * string, i32 num);
void u32_to_ascii(char * string, u32 num);
bool string_compare(const char * string1, const char * string2);

#endif
