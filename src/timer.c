#include "timer.h"
#include "io.h"
#include "string.h"
#include "vga.h"
#include "log.h"
#include "irq.h"
#include "pic.h"

// 0x36: 0011 0110: read the basic usage on timer.h
u32 tick = 0;

void init_timer(u32 frequency) {
    // add_handler(IRQ0, (u32)handler_int32);
    u32 divisor = (u32)(1193180/frequency);
    u8 dividor_low = (u8)(divisor & 0xFF);
    u8 dividor_high = (u8)((divisor >> 8) & 0xFF);
    outb(TIMER_COMMAND_PORT, 0x36);
    outb(TIMER_DATA_PORT_0, dividor_low);
    outb(TIMER_DATA_PORT_0, dividor_high);
}

void timer_callback() {
    tick++;
    PIC_send_EOI(IRQ0);
}

u32 get_tick() {
    return tick;
}


void wait(u32 time) {
    u32 current_tick = tick;
    while (tick < current_tick + time) {
    //     char buffer[32];
    //     // i32_to_ascii(buffer, tick);
    //     // kprint(buffer);
        
    }
}
