#include "vga.h"
#include "io.h"
#include "memutils.h"
#include "string.h"

#define SCREEN_WIDTH 80
#define SCREEN_HEIGHT 25
#define VGA_BUFFER_ADDR 0xb8000

VGA_color current_background_color = Black;
VGA_color current_foreground_color = White;

void change_screen_color_bg(VGA_color color) {
    current_background_color = color;
    for (int i=1; i<SCREEN_HEIGHT*SCREEN_HEIGHT*2; i+=2) {
	*((u8*)VGA_BUFFER_ADDR+i)=(current_foreground_color&0xf) << 4 | current_background_color;
    }
}

void enable_cursor(u8 cursor_start, u8 cursor_end)
{
	outb(VGA_COMMAND_PORT, 0x0A);
	outb(VGA_DATA_PORT, (inb(VGA_DATA_PORT) & 0xC0) | cursor_start);
 
	outb(VGA_COMMAND_PORT, 0x0B);
	outb(VGA_DATA_PORT, (inb(VGA_DATA_PORT) & 0xE0) | cursor_end);
}

void scroll_up() {
    u8 * framebuffer = (u8*) VGA_BUFFER_ADDR;
    for (int i=1; i<=SCREEN_HEIGHT; i++) {
        memory_copy(framebuffer+(SCREEN_WIDTH*2)*(i-1), framebuffer+(SCREEN_WIDTH*2)*i, SCREEN_WIDTH*2);
    }
}


u16 get_cursor_position() {
    u16 i = 0;
    outb(VGA_COMMAND_PORT, 0xf);
    i = inb(VGA_DATA_PORT);
    outb(VGA_COMMAND_PORT, 0xe);
    i |= ((u16)inb(VGA_DATA_PORT)) << 8;
    return i;
}

void set_cursor_position(u16 i) {
    outb(VGA_COMMAND_PORT, 0xe);
    outb(VGA_DATA_PORT, (u8)((i >> 8) & 0xff));
    outb(VGA_COMMAND_PORT, 0xf);
    outb(VGA_DATA_PORT, (u8)(i & 0xff));
}

i16 coord_to_fb_pos(u8 x, u8 y) {
    if (x >= SCREEN_WIDTH || y >= SCREEN_HEIGHT) {
        // Some kind of error handling....
        return -1;
    }
    return 2*(x+y*SCREEN_WIDTH);
}

/** framebuffer write cell
 *  Writes a character (c) at the position i
 * @param i     The character position in coordinates the screen understands
 * @param c     The ascii character to be displayed
 * @param fg    The foreground color
 * @param bg    The background color
 * */
void fb_write_cell(u16 i, char c, VGA_color bg, VGA_color fg) {
    u8 * framebuffer = (u8*) VGA_BUFFER_ADDR;
    *(framebuffer+i) = c;
    *(framebuffer+i+1) = ((fg & 0xf) << 4) | (bg & 0xf);
}

void print_char(char c) {
    fb_write_cell(get_cursor_position()*2, c, Black, White);
    set_cursor_position(get_cursor_position()+1);
}

/** kprint
 * Prints a string to the screen from where the cursor is
 * @param string    The null delimited string to be printed
 * @param len       The length of the string to be printed
 * */
void kprint(char const * string) {
    u32 len = string_length(string);
    u16 pos = get_cursor_position();
    kprint_at(pos, current_background_color, current_foreground_color, string, len);
}

/** kprint at pos
 * Prints a string to the screen from where the cursor is
 * @param x         The column number in screen
 * @param y         The row number in screen
 * @param fg        The color of text written
 * @param bg        The color of the background of the text written
 * @param string    The null delimited string to be printed
 * @param len       The length of the string to be printed
 * */
void kprint_at_pos(u8 x, u8 y, VGA_color bg, VGA_color fg, char const * string, u32 len) {
    u16 i = x + SCREEN_WIDTH*y;
    kprint_at(i, bg, fg, string, len);
}

/** kprint_at
 * Prints a string to the screen from where the cursor is
 * @param i         The coordiates in framebuffer
 * @param fg        The color of text written
 * @param bg        The color of the background of the text written
 * @param string    The null delimited string to be printed
 * @param len       The length of the string to be printed
 * */
void kprint_at(u16 i, VGA_color bg, VGA_color fg, char const * string, u32 len) {
    u32 j=0;
    for (j = 0; j<len; j++) {
        if ((i+j) >= SCREEN_WIDTH*SCREEN_HEIGHT) {
            scroll_up();
            i-=(SCREEN_WIDTH);
        }
        char c =  *(string+j);
        switch (c) {
            case '\n':
                i+=SCREEN_WIDTH;
                i-=(i+j+1)%SCREEN_WIDTH; // The +1 is to ignore the character \n
                continue;
                break;
            case '\b':
                i-=1;
                fb_write_cell((i+j)*2, '\0', bg, fg);
                i-=1;
                continue;
                break;
            case '\t':
                i+=TAB_SIZE;
                continue;
                break;
            default:
                fb_write_cell((i+j)*2, c, bg, fg);
                break;
        }
    }
    set_cursor_position(i+j);
}
