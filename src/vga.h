#ifndef VGA__H
#define VGA__H

#include "types.h"

#define VGA_COMMAND_PORT 0x3d4
#define VGA_DATA_PORT 0x3d5

#define TAB_SIZE 4

typedef enum {
    Black,
    Blue,
    Green,
    Cyan,
    Red,
    Magenta,
    Brown,
    LightGrey,
    DarkGrey,
    LightBlue,
    LightGreen,
    LightCyan,
    LightRed,
    LightMagenta,
    LightBrown,
    White
} VGA_color;

void enable_cursor(u8 cursor_start, u8 cursor_end);
/** framebuffer write cell
 *  Writes a character (c) at the position i
 * @param i     The character position in coordinates the screen understands
 * @param c     The ascii character to be displayed
 * @param fg    The foreground color
 * @param bg    The background color
 * */
void fb_write_cell(u16 i, char c, VGA_color fg, VGA_color bg) ;

/** kprint
 * Prints a string to the screen from where the cursor is
 * @param string    The null delimited string to be printed
 * */
void kprint(char const * string);

/** kprint_at
 * Prints a string to the screen from where the cursor is
 * @param i         The coordiates in framebuffer
 * @param fg        The color of text written
 * @param bg        The color of the background of the text written
 * @param string    The null delimited string to be printed
 * @param len       The length of the string to be printed
 * */
void kprint_at(u16 i, VGA_color fg, VGA_color bg, char const * string, u32 len);

/** kprint at pos
 * Prints a string to the screen from where the cursor is
 * @param x         The column number in screen
 * @param y         The row number in screen
 * @param fg        The color of text written
 * @param bg        The color of the background of the text written
 * @param string    The null delimited string to be printed
 * @param len       The length of the string to be printed
 * */
void kprint_at_pos(u8 x, u8 y, VGA_color fg, VGA_color bg, char const * string, u32 len);
void print_char(char c);
void set_cursor_position(u16 i);
u16 get_cursor_position();
void change_screen_color_bg(VGA_color color);
#endif
